<?php

include_once('settings.php');

if (!$DEBUG) {
  include('top-cache.php'); 
}

include_once('campaignthermometer.class.php');

# Local settings
$campaigns = [1, 26];

$ct = new CampaignThermometer($api_root, $user_api, $pwd_api, $shift, $goal_amount, $year);

# API calls
$ct->get_total_amount($campaigns);


//*/
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Soutenez-nous !</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="lib/bootstrap-4.5.0-dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="iframe.css" />
    <link rel="stylesheet" href="circle.css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div id="donations-thermometer">
  		<h3>On a besoin de vous !</h3>
<?php if ($ct->data_available) { ?>
		<div class="progresscircle" data-percentage=<?php echo $ct->percentage; ?>>
			<span class="progresscircle-left">
				<span class="progresscircle-bar"></span>
			</span>
			<span class="progresscircle-right">
				<span class="progresscircle-bar"></span>
			</span>
			<div class="progresscircle-value font-weight-bold text-primary">
				<div>
					<?php echo $ct->percentage; ?>%
				</div>
			</div>
		</div>

  		<div class="row text-center mt-4">
          <div class="col-6 border-right">
            <div class="h4 font-weight-bold mb-0 text-primary"><?php echo $ct->show_current_amount(); ?></div><span class="small text-gray">/ <?php echo number_format($goal_amount, 0, ',', ' '); ?> €</span>
          </div>
          <?php } ?>

          <?php if (time() < $ct->date) { ?>
          <div class="col-6">
          	<?php if ($ct->days > 0) { ?>
            <div class="h4 font-weight-bold mb-0 text-primary"><?php echo $ct->days ?></div><span class="small text-gray">jours restants</span>
        	<?php } else { ?>
        		<div class="h4 font-weight-bold mb-0 text-primary"><?php echo $ct->show_remaining_hourmin(); ?></div><span class="small text-gray">restantes</span>
        	<?php } ?>
          </div>
        </div>
    	<?php } ?>

  		<p class="calltoaction">Wikimédia France ne vit que grâce à vos dons ! Pour que nous puissions continuer à soutenir la connaissance libre en <?php echo($year+1) ?>,
  		<strong><a href="http://dons.wikimedia.fr/soutenez-nous/" target="_blank" title="Soutenez-nous">soutenez-nous !</a></strong></p>
	</div>
	<script src="lib/jquery-3.5.1.min.js"></script>
	<script src="lib/bootstrap-4.5.0-dist/js/bootstrap.min.js"></script>
  </body>
</html>

<?php 
if (!$DEBUG) {
  include('bottom-cache.php'); 
}
?>
